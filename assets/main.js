var Location = React.createClass({
	getInitialState : function(){
		return{
			fov: 120,
			heading: 1,
			pitch: 1,
			latitude: 1,
			longitude: 1,

		}
	},
	find :function (){
		navigator.geolocation.getCurrentPosition(this.success, this.error);
	},
	error : function(){
		this.setState({

		})
	},
	success : function (position){
		var latitude  = position.coords.latitude;
		var longitude = position.coords.longitude;
		this.setState({
			latitude:  latitude,
			longitude:  longitude,
		})
	},

	fov :function(event){
		var fovint = event.target.value;
		this.setState({
			fov:fovint
		});
	},
	heading :function(event){
		var headingint = event.target.value;
		this.setState({
			heading:headingint
		});
	},
	pitch :function(event){		
		var pitchint = event.target.value;
		this.setState({
			pitch:pitchint
		});
	},
	nota :function(event){		
		var notastr = event.target.value;
		this.setState({
			nota:notastr
		});
	},

	save : function(){
		var form = this.state
		

	},
	render	: function	(){
		this.find();
		var fov = this.state.fov;
		var heading = this.state.heading;
		var pitch = this.state.pitch;
		var latitude = this.state.latitude;
		var longitude = this.state.longitude;
		var nota = this.state.nota;
		var street = "https://maps.googleapis.com/maps/api/streetview?size=600x300&location="+latitude+","+longitude+"&fov="+ fov+"&heading="+ heading+"&pitch="+ pitch+ "&key=AIzaSyBesUqK-_RAwd1zWzxFIiLPKRsdH7bBwDQ";
		return(
			<div className="card-panel">
				<div className="divider"></div>
				<div className="row center">
					<div className="col s12">
						<img src={street} />
					</div>
				</div>
				<div className="row">
					<div className="col s12">
							<input type="range" step='30' id="heading" min="-135" max="135" value={heading} onChange={this.heading} />
							<input type="range" step='30' id="pitch" min="-30" max="30" value={pitch} onChange={this.pitch} />
					</div>
				</div>
				<div className="row">
					<div class="input-field col s12">
						<label for="nota" className='active'>Nota</label>
						<textarea id="nota" class="materialize-textarea" length="200" value={nota} onChange={this.nota}></textarea>
						<button className='btn' onClick={this.save}>Guardar</button>
					</div>

				</div>
			</div>
		)
	},

});



React.render (<Location/>,document.getElementById('hello'))
